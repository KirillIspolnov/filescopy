package org.kllbff.edu.fcopy.launch;

import org.kllbff.edu.fcopy.FileCopier;

public class AsyncLauncher {
 
    public static void main(String[] args) throws InterruptedException {
        FileCopierThread copierThread1 = new FileCopierThread(new FileCopier("text.txt", "text1.txt"));
        FileCopierThread copierThread2 = new FileCopierThread(new FileCopier("image.png", "image1.png"));
        
        long time = System.currentTimeMillis();
        
        copierThread1.start();
        copierThread2.start();
        
        copierThread1.join();
        copierThread2.join();    
        
        time = System.currentTimeMillis() - time;
        
        System.out.printf("Успел за %d мс", time);
    }

}

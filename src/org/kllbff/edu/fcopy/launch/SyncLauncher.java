package org.kllbff.edu.fcopy.launch;

import java.io.IOException;

import org.kllbff.edu.fcopy.FileCopier;

public class SyncLauncher {
 
    public static void main(String[] args) throws IOException {
        FileCopier copier1 = new FileCopier("text.txt", "text1.txt");
        FileCopier copier2 = new FileCopier("image.png", "image1.png");
        
        long time = System.currentTimeMillis();
        copier1.copy();
        copier2.copy();
        time = System.currentTimeMillis() - time;
        
        System.out.printf("Успел за %d мс", time);
    }

}

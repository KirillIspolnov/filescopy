package org.kllbff.edu.fcopy.launch;

import org.kllbff.edu.fcopy.FileCopier;

public class FileCopierThread extends Thread {
    private FileCopier myCopier;
    
    public FileCopierThread(FileCopier copier) {
        myCopier = copier;
    }
    
    public void run() {
        myCopier.copy();
    }
}

package org.kllbff.edu.fcopy;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Позволяет копировать указанный файл по указанному пути
 * 
 * @author К. Испольнов, 16ит18К
 */
public class FileCopier {
    /* Копирование успешно, ошибок и предупреждений нет */
    public static final int OK                            = 0x00,
    /* Исходный и конечный файл - один и тот же файл */
                            THE_SAME_FILE                 = 0x02,
    /* Исходный файл не существует */
                            SOURCE_FILE_DOES_NOT_EXISTS = 0x04,
    /* Конечный файл существует и будет перезаписан */
                            TARGET_FILE_EXISTS             = 0x08,
    /* Ошибка ввода-вывода при копировании */
                            IO_EXCEPTION                 = 0x10;
    
    /*
     * Исходный файл 
     */
    private File source;
    /*
     * Конечный файл (копия)
     */
    private File target;
    
    /**
     * Инициализирует объект используя полученные путь к исходному файлу
     * и путь к файлу копии
     * 
     * @param source путь к исходному файлу
     * @param target путь к будущей копии
     */
    public FileCopier(String source, String target) {
        this.source = new File(source);
        this.target = new File(target);
    }
        
    /**
     * Выполняет копирование исходного в конечный файл
     *  
     * @return результат копирования, представленный один из флагов
     */
    public int copy() {
        int result = OK;
        
        if(target.exists()) {
            result = TARGET_FILE_EXISTS;
        }
        if(source.equals(target)) {
            return THE_SAME_FILE;
        }
        if(!source.exists()) {
            return SOURCE_FILE_DOES_NOT_EXISTS;
        }
        
        try(InputStream in = new BufferedInputStream(new FileInputStream(source));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(target))){
            
            //буфер - сюда будем читать файл порциями
            byte[] buffer = new byte[1024];
            //кол-во прочитанный байт
            int count;
            //когда файл закончится будет прочитано 0 байт, а метод read вернет значение -1
            while((count = in.read(buffer)) != -1) {
                //запись прочитанных данных в конечный файл
                out.write(buffer, 0, count);
                //а вдруг произойдет ошибочка?
                //давайте заставим систему записать данные на винт прямо сейчас
                out.flush();
            }
        } catch(IOException ioe) {
            result = IO_EXCEPTION;
        }
        
        return result;
    }
    
    @Override
    public String toString() {
        return source.getPath() + " > " + target.getPath();
    }
}

package org.kllbff.edu.fcopy;

import static org.junit.Assert.*;
import static org.kllbff.edu.fcopy.FileCopier.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Test;

public class FileCopierTest {
	@Test
	public void testCheckSameFile() {
		FileCopier copier = new FileCopier("text.txt", "text.txt");
		assertEquals(THE_SAME_FILE, copier.copy());
	}
	
	@Test
	public void testCheckingSrcNotExists() {
		FileCopier copier = new FileCopier("text-lol.txt", "text-kek.txt");
		assertEquals(SOURCE_FILE_DOES_NOT_EXISTS, copier.copy());
	}
	
	@Test
	public void testCheckingOk() {
		FileCopier copier = new FileCopier("text.txt", "copy.txt");
		assertEquals(OK, copier.copy());
	}
	
	@Test
	public void testCheckingRewriteTarget() {
		FileCopier copier = new FileCopier("big-text.txt", "text.txt");
		assertEquals(TARGET_FILE_EXISTS, copier.copy());
	}
	
	@Test
	public void testCopySmallTxt() throws IOException {
		FileCopier copier = new FileCopier("text.txt", "text-out.txt");
		copier.copy();
		assertTrue(checkContent("text.txt", "text-out.txt"));
	}
	
	@Test
	public void testCopyBigTxt() throws IOException {
		FileCopier copier = new FileCopier("big-text.txt", "big-copy.txt");
		copier.copy();
		assertTrue(checkContent("big-text.txt", "big-copy.txt"));
	}
	
	@Test
	public void testCopyBin() throws IOException {
		FileCopier copier = new FileCopier("image.png", "image-copy.png");
		copier.copy();
		assertTrue(checkContent("image.png", "image-copy.png"));
	}
	
	@Test
	public void testSpeed() {
		double speed = 0.0;
		FileCopier copier = new FileCopier("image.png", "image-copy.png");
		for(int i = 0; i < 1_000; i++) {
			long start = System.currentTimeMillis();
			copier.copy();
			speed += (System.currentTimeMillis() - start);
			System.out.println("SpeedTest (" + (i + 1) + " / 1000)");
		}
		speed /= 1_000;
		System.out.println(speed);
		assertTrue(speed < 120);
	}

	private boolean checkContent(String src, String trg) throws IOException {
		byte[] source = Files.readAllBytes(Paths.get(src));
		byte[] target = Files.readAllBytes(Paths.get(trg));
		return Arrays.equals(source, target);
	}
}
